Feature('login');
//npx codeceptjs run --steps <-- para executar os testes

Scenario('Login com sucesso',  ({ I }) => {
    I.amOnPage('https://automationpratice.com.br/');
    I.click('Login');
    I.waitForText('Login', 10);
    I.fillField('#user','iago@qazando.com');
    I.click('#password');
    I.fillField('#password', '123456789');
    I.click('#btnLogin');
    I.waitForText('Login realizado', 3);

}).tag('@sucesso');

Scenario('Tentando Logar digitando apenas o e-mail',  ({ I }) => {
    I.amOnPage('https://automationpratice.com.br/');
    I.click('Login');
    I.waitForText('Login', 10);
    I.fillField('#user','iago@teste.com');
    I.click('#btnLogin');
    I.waitForText('Senha inválida.', 3);
   
}).tag('@dadosInvalidos');

Scenario('Tentando logar sem digitar e-mail e senha',  ({ I }) => {
    I.amOnPage('https://automationpratice.com.br/');
    I.click('Login');
    //I.waitForText('Login', 10);
    I.click('#btnLogin');
    I.waitForText('E-mail inválido.', 3);

}).tag('@dadosInvalidos');

Scenario('Tentando Logar digitando apenas a senha',  ({ I }) => {
    I.amOnPage('https://automationpratice.com.br/');
    I.click('Login');
    //I.waitForText('Login', 10);
    I.click('#password');
    I.fillField('#password', '123456789');
    I.click('#btnLogin');
    I.waitForText('E-mail inválido.', 3);

}).tag('@dadosInvalidos');